/*
 * Public API Surface of ng-scorm-player
 */

export * from './lib/model/ng-scorm-player-config';
export * from './lib/model/scorm-result';
export * from './lib/ng-scorm-player.service';
export * from './lib/ng-scorm-player.component';
export * from './lib/ng-scorm-player.module';
