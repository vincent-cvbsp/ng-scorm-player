import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgScormPlayerComponent } from './ng-scorm-player.component';
import { HttpClient } from '@angular/common/http';
import { HttpClientMock } from './mock/http-client-mock';

describe('NgScormPlayerComponent', () => {
  let component: NgScormPlayerComponent;
  let fixture: ComponentFixture<NgScormPlayerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NgScormPlayerComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [{ provide: HttpClient, useClass: HttpClientMock }]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NgScormPlayerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
