import { TestBed } from '@angular/core/testing';

import { NgScormPlayerService } from './ng-scorm-player.service';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { HttpClientMock } from './mock/http-client-mock';

describe('NgScormPlayerService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [{ provide: HttpClient, useClass: HttpClientMock }]
  }));

  it('should be created', () => {
    const service: NgScormPlayerService = TestBed.get(NgScormPlayerService);
    expect(service).toBeTruthy();
  });
});
