import { Injectable, Optional } from '@angular/core';
import { ScormResult } from './model/scorm-result';
import { Subject, Observable, of } from 'rxjs';
import { NgScormPlayerConfig } from './model/ng-scorm-player-config';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class NgScormPlayerService {

  runtimeData: any;
  scormResult: ScormResult;
  closeWindowOnFinish = false;
  errorLogs: any = [];
  debug: boolean;
  urlLaunchPage: string;
  openInNewWindow  = false;
  // Events
  private initializeSource = new Subject<ScormResult>();
  initializeEvent = this.initializeSource.asObservable();
  private setValueSource = new Subject<ScormResult>();
  setValueEvent = this.setValueSource.asObservable();
  private getValueSource = new Subject<ScormResult>();
  getValueEvent = this.getValueSource.asObservable();
  private finishSource = new Subject<ScormResult>();
  finishEvent = this.finishSource.asObservable();
  private commitSource = new Subject<ScormResult>();
  commitEvent = this.commitSource.asObservable();

  constructor(
    @Optional() config: NgScormPlayerConfig,
    private http: HttpClient
  ) {
    if (config) {
      this.debug = config.debug;
      this.openInNewWindow = config.openInNewWindow;
    }
  }

  // HELPERS
  /**
   * read the manifest and get the url launch page
   * @param urlDirSco url of the directory of the scorm
   */
  getUrlLaunchPage(urlDirSco: string): Observable<string> {
    const url = `${urlDirSco}/imsmanifest.xml`;
    return this.http.get(url, {responseType: 'text'}).pipe(map(res => {
      let launchUrl = '';
      const parser = new DOMParser();
      const doc = parser.parseFromString(res, 'text/xml');
      const resources = doc.getElementsByTagName('resource');
      const filename = resources[0].getAttribute('href');
      launchUrl = `${urlDirSco}/${filename}`;
      return launchUrl;
    }));
  }

  // SCORM 2004
  Initialize() {
    return this.MyInitialize();
  }

  SetValue(key, value) {
    return this.MySetValue(key, value);
  }

  GetValue(key) {
    return this.MyGetValue(key);
  }

  Terminate() {
    return this.MyFinish(); // return false to avoid refreh in some  cases
  }

  Commit(): boolean {
    return this.MyCommit();
  }

  GetLastError() {
    return this.MyGetLastError();
  }

  GetErrorString(errorCode: any) {
    return this.MyGetErrorString(errorCode);
  }

  GetDiagnostic(errorCode: any) {
    return this.MyGetDiagnostic(errorCode);
  }

  // SCORM 1.2
  LMSInitialize(): boolean {
    return this.MyInitialize();
  }

  LMSFinish(data): boolean {
    return this.MyFinish(); // return false to avoid refreh in some  cases
  }

  LMSGetValue(key: string): string {
   return this.MyGetValue(key);
  }

  LMSSetValue( key: any, value: string): string {
    return this.MySetValue(key, value);
  }

  LMSCommit(): boolean {
    return this.MyCommit();
  }

  LMSGetLastError(): any {
    return this.MyGetLastError();
  }

  LMSGetErrorString( errorCode: any ): string {
    return this.MyGetErrorString(errorCode);
  }

  LMSGetDiagnostic( errorCode: any ): string {
    return this.MyGetDiagnostic(errorCode);
  }

  // My Method
  MyInitialize(): boolean {
    try {
      if (!this.runtimeData) {
        this.runtimeData = {};
      }

      if (!this.scormResult) {
        this.scormResult = new ScormResult();
      }
      this.initializeSource.next(this.scormResult);
      return true;
    } catch (err) {
      this.logErrorCode(3);
      return false;
    }
  }

  MyFinish(): boolean {
    try {
      if (this.runtimeData['cmi.completion_status'] === 'completed') {
        this.scormResult.completedOn = new Date();
      }
      this.scormResult.timeSpent = this.parseDurationString(this.runtimeData['cmi.session_time']);
      // progess mesure is value between 0 and 1
      this.scormResult.progression = this.runtimeData['cmi.progress_measure'] ? this.runtimeData['cmi.progress_measure'] * 100 : null;
      this.scormResult.scorePercent = this.runtimeData['cmi.score.scaled'] ? this.runtimeData['cmi.score.scaled'] * 100 : null;
      this.scormResult.runtimeData = this.runtimeData;
      this.finishSource.next(this.scormResult);

      return true;
    } catch (err) {
      this.logErrorCode(5);
      return true;
    }

  }

  MyGetValue(key: string): string {
    try {
      this.getValueSource.next(this.scormResult);
      return this.runtimeData[key] ? this.runtimeData[key] : '';
    } catch (err) {
      this.logErrorCode(1);
      return '';
    }
  }

  MySetValue( key: any, value: string): string {
    try {
      this.runtimeData[key] = value;
      this.scormResult.runtimeData = this.runtimeData;
      this.setValueSource.next(this.scormResult);
      return value;
    } catch (err) {
      this.logErrorCode(2);
      return value;
    }

  }

  MyCommit(): boolean {
    try {
      this.scormResult.timeSpent = this.parseDurationString(this.runtimeData['cmi.session_time']);
      // progess mesure is value between 0 and 1
      this.scormResult.progression = this.runtimeData['cmi.progress_measure'] ? this.runtimeData['cmi.progress_measure'] * 100 : null;
      this.scormResult.scorePercent = this.runtimeData['cmi.score.scaled'] ? this.runtimeData['cmi.score.scaled'] * 100 : null;
      this.scormResult.runtimeData = this.runtimeData;
      this.commitSource.next(this.scormResult);
      return true;
    } catch (err) {
      this.logErrorCode(6);
      return false;
    }
  }

  MyGetLastError(): any {
    return this.errorLogs.length > 0 ? this.errorLogs[0] : 0;
  }

  MyGetErrorString( errorCode: any ): string {
    let errorString;
    switch (errorCode) {
      case 0:
          errorString = 'None error';
          break;
      case 1:
          errorString = 'Error to get value';
          break;
      case 2:
          errorString = 'Error to set value';
          break;
      case 3:
          errorString = 'Error to Initialize';
          break;
      case 4:
          errorString = 'Error to save data';
          break;
      case 5:
          errorString = 'Error to Finish';
          break;
      case 6:
          errorString = 'Error to Commit';
          break;
    }
    return errorString;
  }

  MyGetDiagnostic( errocCode: any ): string {
    return 'NO_DIAGNOSTIC';
  }

  // helpers

  private logErrorCode(msg: number) {
    this.errorLogs.push(msg);
  }

  private parseDurationString( durationString ) {
    if (!durationString || durationString === '') {
      return 0;
    }
    const stringPattern = /^PT(?:(\d+)D)?(?:(\d+)H)?(?:(\d+)M)?(?:(\d+(?:\.\d{1,3})?)S)?$/;
    const stringParts = stringPattern.exec( durationString );
    return (
             (
               (
                 ( stringParts[1] === undefined ? 0 : Number(stringParts[1]) )  /* Days */
                 * 24 + ( stringParts[2] === undefined ? 0 : Number(stringParts[2]) ) /* Hours */
               )
               * 60 + ( stringParts[3] === undefined ? 0 : Number(stringParts[3]) ) /* Minutes */
             )
             * 60 + ( stringParts[4] === undefined ? 0 : Number(stringParts[4]) ) /* Seconds */
           );
  }
}
