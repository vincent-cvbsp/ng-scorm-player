import { NgModule, ModuleWithProviders } from '@angular/core';
import { NgScormPlayerConfig } from './model/ng-scorm-player-config';
import { NgScormPlayerComponent } from './ng-scorm-player.component';
import { CommonModule } from '@angular/common';
import { WindowRef } from './window-ref.service';

@NgModule({
  declarations: [NgScormPlayerComponent],
  imports: [
    CommonModule
  ],
  exports: [
    NgScormPlayerComponent
  ],
  providers: [
    WindowRef
  ]

})

export class NgScormPlayerModule {
    static forRoot( config: NgScormPlayerConfig ): ModuleWithProviders {
      return {
        ngModule: NgScormPlayerModule,
        providers: [
          {provide: NgScormPlayerConfig, useValue: config }
        ]
      };
    }

    static forChild( config: NgScormPlayerConfig ): ModuleWithProviders {
      return {
        ngModule: NgScormPlayerModule,
        providers: [
          {provide: NgScormPlayerConfig, useValue: config }
        ],
      };
    }
}
