import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { NgScormPlayerService } from './ng-scorm-player.service';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { ScormResult } from './model/scorm-result';
import { WindowRef } from './window-ref.service';

@Component({
  selector: 'ng-scorm-player',
  templateUrl: './ng-scorm-player.component.html',
  styleUrls: ['./ng-scorm-player.component.css'],
})
export class NgScormPlayerComponent implements OnInit, OnChanges {

  API: any;
  urlLaunchPageSafe: SafeUrl;
  @Input() urlDirSco: string;
  @Input() urlLaunchPage: string;
  @Input() scormResult: ScormResult;
  @Input() openInNewWindow: boolean;
  initializing: any;
  constructor(
    private win : WindowRef,
    public playerSco: NgScormPlayerService,
    public sanitizer: DomSanitizer
  ) {
    this.openInNewWindow = this.openInNewWindow  ? this.openInNewWindow  : this.playerSco.openInNewWindow
  }

  ngOnInit(): void {
    this.init();
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.init();
    if(this.playerSco.debug) {
      console.log('prop changes:', changes);
    }
  }

  init() {
    if (this.initializing) {
      return;
    }
    this.initializing = true;
    this.win.nativeWindow.API_1484_11 = this.playerSco;
    this.win.nativeWindow.API = this.playerSco;
    // try to set the api directly in the frame window
    try {
      var frame = document.getElementById("scoIframe");
      frame['contentWindow'].API_1484_11 = this.playerSco;
      frame['contentWindow'].API = this.playerSco;
    } catch (err) {
      if(this.playerSco.debug) {
        console.log('error settings api directlty in frame:', err);
      }
    }
    
    if (!this.urlLaunchPage && this.urlDirSco) {
      this.playerSco.getUrlLaunchPage(this.urlDirSco).subscribe(url => {
        this.urlLaunchPageSafe = this.sanitizer.bypassSecurityTrustResourceUrl(url);
        this.maybeOpenWindow(url);
        this.initializing = false;
      }, err => {
        if (this.playerSco.debug) {
          console.log('error finding urlLaunchPage', err);
        }
        this.initializing = false;
      });
    } else if (this.urlLaunchPage) {
      this.urlLaunchPageSafe = this.sanitizer.bypassSecurityTrustResourceUrl(this.urlLaunchPage);
      this.maybeOpenWindow(this.urlLaunchPage);
      this.initializing = false;
    } else {
      if(this.playerSco.debug) {
        console.error('no url provided');
      }
      this.initializing = false;
    }    
  }

  maybeOpenWindow(url: string) {
    if (this.openInNewWindow) {
      this.win.nativeWindow.open(url, '_blank');
    }
  }

}
