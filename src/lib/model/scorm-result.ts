export class ScormResult {
    timeSpent: number;
    progression: number;
    completed: boolean;
    completedOn: Date;
    score: number;
    scoreMax: number;
    scorePercent: number;
    /** raw scorm data */
    runtimeData: any;

}
