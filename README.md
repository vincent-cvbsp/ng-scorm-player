# NgScormPlayer

This library was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.2.0.

install :

npm i ng-scorm-player

add the module to your project :
@NgModule({
  imports: [
    ...
    NgScormPlayerModule.forChild({debug : true}),
  ],
  declarations: [...]
})

add the component to your page, you must populate either [urlDirSco] or [urlLaunchPage]. urlDirSco will read the manifest to find the launchPage, it is useful if you don't know the launch page :

<ng-scorm-player [urlDirSco]="'urlDirOfSco'" [urlLaunchPage]="'urlLaunchPage'" [scormResult]=""></ng-scorm-player>

IMPORTANT : the url must be from the same origin ( limit of the iframe )

get events with the following observables :

this.player.initializeEvent.subscribe(val => {
    console.log('initializeEvent:', val);
});
this.player.setValueEvent.subscribe(val => {
    console.log('setValueEvent:', val);
});
this.player.getValueEvent.subscribe(val => {
    console.log('getValueEvent:', val);
});
this.player.finishEvent.subscribe(val => {
    console.log('finishEvent:', val);
});
this.player.commitEvent.subscribe(val => {
    console.log('commitEvent:', val);
});

## Code scaffolding

Run `ng generate component component-name --project ng-scorm-player` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module --project ng-scorm-player`.
> Note: Don't forget to add `--project ng-scorm-player` or else it will be added to the default project in your `angular.json` file. 

## Build

Run `ng build ng-scorm-player` to build the project. The build artifacts will be stored in the `dist/` directory.

## Publishing

After building your library with `ng build ng-scorm-player`, go to the dist folder `cd dist/ng-scorm-player` and run `npm publish`.

## Running unit tests

Run `ng test ng-scorm-player` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
